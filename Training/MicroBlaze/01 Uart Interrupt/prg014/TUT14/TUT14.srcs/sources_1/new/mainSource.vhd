----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02/22/2019 09:13:18 AM
-- Design Name: 
-- Module Name: mainSource - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity mainSource is
    Port (  clk : in STD_LOGIC;
            RX : in STD_LOGIC;
            TX : out STD_LOGIC;
            LED0 : out STD_LOGIC;
            LED1 : out STD_LOGIC);
end mainSource;

architecture Behavioral of mainSource is
component design_1_wrapper is
  port (
    clk_25MHz : in STD_LOGIC;
    gpio_rtl_0_tri_o : out STD_LOGIC_VECTOR ( 1 downto 0 );
    reset_rtl_0 : in STD_LOGIC;
    uart_rtl_0_rxd : in STD_LOGIC;
    uart_rtl_0_txd : out STD_LOGIC
  );
end component;
signal  LED :   STD_LOGIC_VECTOR(1 downto 0 );
   
begin

A1: design_1_wrapper port map(clk,LED,'1',RX,TX);

end Behavioral;
